import 'package:flutter/material.dart';

import '../main.dart';

class RightArrow extends StatelessWidget {
  final PageController _pageController;

  RightArrow(this._pageController);

  @override
  Widget build(BuildContext context) {
    int actualPage = _pageController.page != null ? _pageController.page.toInt() : 0;
    return Opacity(
      child: Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: Align(
          alignment: AlignmentDirectional.centerEnd,
          child: new Material(
            color: Colors.grey[600],
            type: MaterialType.circle,
            elevation: 6.0,
            child: new InkWell(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onTap: () {},
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: new Icon(
                  Icons.chevron_right,
                  color: Colors.white,
                  size: 16,
                ),
              ),
            ),
          ),
        ),
      ),
      opacity: actualPage + 1 < max ? 1 : 0,
    );
  }
}
