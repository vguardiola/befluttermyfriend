import 'package:flutter/material.dart';

class LeftArrow extends StatefulWidget {
  final PageController _pageController;

  LeftArrow(this._pageController);

  @override
  _LeftArrowState createState() => _LeftArrowState();
}

class _LeftArrowState extends State<LeftArrow> {
  @override
  Widget build(BuildContext context) {
    int actualPage = widget._pageController.page != null ? widget._pageController.page.toInt() : 0;
    return Padding(
      padding: const EdgeInsets.only(left: 8.0),
      child: Align(
        alignment: AlignmentDirectional.centerStart,
        child: Opacity(
          child: new Material(
            color: Colors.grey[600],
            type: MaterialType.circle,
            elevation: 6.0,
            child: new InkWell(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onTap: () {},
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: new Icon(
                  Icons.chevron_left,
                  color: Colors.white,
                  size: 16,
                ),
              ),
            ),
          ),
          opacity: actualPage - 1 >= 0 ? 1 : 0,
        ),
      ),
    );
  }
}
