import 'package:flutter/material.dart';

import '../main.dart';

class PageOfPage extends StatelessWidget {
  final PageController _pageController;
  PageOfPage(this._pageController);

  @override
  Widget build(BuildContext context) {
    int actualPage = _pageController.page != null ? _pageController.page.toInt() + 1 : 1;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Align(
        alignment: AlignmentDirectional.bottomEnd,
        child: new Material(
          color: Colors.grey[600],
          type: MaterialType.circle,
          elevation: 6.0,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Text(
              '$actualPage/$max',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
