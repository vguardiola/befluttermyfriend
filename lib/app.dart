import 'package:be_flutter_my_friend/slides/first_slide.dart';
import 'package:be_flutter_my_friend/slides/four_slide.dart';
import 'package:be_flutter_my_friend/slides/second_slide.dart';
import 'package:be_flutter_my_friend/slides/third_slide.dart';
import 'package:be_flutter_my_friend/widgets/left_arrow.dart';
import 'package:be_flutter_my_friend/widgets/page_of_page.dart';
import 'package:be_flutter_my_friend/widgets/right_arrow.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'main.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final int page = 1;
  int _page = 0;

  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = new PageController(initialPage: 0);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: new RawKeyboardListener(
        child: Stack(
          children: <Widget>[
            new PageView(
              controller: _pageController,
              pageSnapping: true,
              children: [
                new FirstSlide(),
                new SecondSlide(),
                new ThirdSlide(),
                new FourSlide(),
              ],
              onPageChanged: (newPage) {
                setState(() {
                  this._page = newPage;
                });
              },
            ),
            LeftArrow(_pageController),
            RightArrow(_pageController),
            PageOfPage(_pageController),
          ],
        ),
        focusNode: new FocusNode(),
        autofocus: true,
        onKey: handleKey,
      ),
    );
  }

  handleKey(RawKeyEvent key) {
    if (key.runtimeType.toString() == 'RawKeyUpEvent') {
      RawKeyEventDataAndroid data = key.data as RawKeyEventDataAndroid;
      String _keyCode = data.keyCode.toString();
      if (_keyCode == "20" || _keyCode == "21") {
        print(_keyCode);
        gotoRoute(-1);
      }
      if (_keyCode == "19" || _keyCode == "22") {
        gotoRoute(1);
        print(_keyCode);
      }
    }
  }

  gotoRoute(direction) {
    int route = _pageController.page.toInt() + direction;
    if (route >= 0 && route < max) {
      _pageController.jumpToPage(route);
    }
  }
}
