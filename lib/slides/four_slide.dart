import 'package:flutter/material.dart';

class FourSlide extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        children: <Widget>[
          Center(
            child: Column(
              children: <Widget>[
                Text(
                  'Show me the code!',
                  style: TextStyle(fontSize: 32, color: Colors.blueAccent),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
