import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:be_flutter_my_friend/widgets/left_arrow.dart';
import 'package:be_flutter_my_friend/widgets/page_of_page.dart';
import 'package:be_flutter_my_friend/widgets/right_arrow.dart';
import 'package:flutter/material.dart';

class SecondSlide extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        children: <Widget>[
          Center(
            child: Column(
              children: <Widget>[
                Text(
                  'Que es Flutter',
                  style: TextStyle(fontSize: 32, color: Colors.blueAccent),
                ),
                Center(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(width: 20.0, height: 100.0),
                      RotateAnimatedTextKit(
                        onTap: () {
                          print("Tap Event");
                        },
                        text: ["AWESOME", "OPTIMISTIC", "DIFFERENT"],
                        textStyle: TextStyle(fontSize: 40.0, fontFamily: "Horizon"),
                        textAlign: TextAlign.start,
                        alignment: AlignmentDirectional.topStart,
                        isRepeatingAnimation: false,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
